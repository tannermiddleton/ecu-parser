﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SplitSourceFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            bool IsCorrectSource = false;
            string strSource = "";
            string baseFileName = "";
            while (IsCorrectSource == false)
            {
                //User Defined Input File and Output Location
                Console.WriteLine("Drag and Drop The Desired Source File And Press ENTER:");
                strSource = Console.ReadLine();
                strSource = strSource.Replace('"', ' ');

                Console.WriteLine();
                Console.WriteLine("Source File Path = " + strSource);
                Console.WriteLine();

                Console.WriteLine("Does The Source File Look Correct? Press [Y/N] ");

                if (Console.ReadLine().ToUpper() == "Y")
                {
                    IsCorrectSource = true;
                    baseFileName = strSource.Substring(strSource.LastIndexOf("\\")).Replace("\\","");
                    break;
                }
            }

            bool IsCorrectDestination = false;
            string strDest = ""; 
            while (IsCorrectDestination == false)
            {

                Console.WriteLine();
                Console.WriteLine("Drag and Drop The Destination Folder For Processed Files And Press ENTER:");
                strDest = Console.ReadLine();

                //Format the directory pathing to be valid 
                strDest = strDest.Replace('"', ' ').TrimEnd() + @"\";

                Console.WriteLine();
                Console.WriteLine("Destinaton Folder = " + strDest);
                Console.WriteLine();
                Console.WriteLine("Does The Destination Path Look Correct? Press [Y/N] ");

                if (Console.ReadLine().ToUpper() == "Y")
                {
                    IsCorrectDestination = true;
                    break;
                }
            }

            List<string> lstSourceText = new List<string>();

            using (StreamReader srSourceLines = new StreamReader(strSource))
            {
                while (srSourceLines.EndOfStream == false)
                {
                    lstSourceText.Add(srSourceLines.ReadLine());

                }
            }
            Console.WriteLine("File Size: " + lstSourceText.Count().ToString());
            int count = 0;
            string strFileName = "";


            string strlastFoot = "";

            //Bundle up each file section in the list and send the list for quicker chunk processing
            List<string> FunctionItems = new List<string>();
            foreach (string strLineText in lstSourceText)
            {
                //FOOTER is paradoxically actually at the beginning of each file
                if (strLineText.ToUpper().Contains("FOOTER"))
                {
                    //The footer contains the file name, so split the footer into an array and extract the filename  
                    string[] arrFooter = strlastFoot.Split('/'.ToString().ToCharArray()); ;

                    foreach (var footSplit in arrFooter)
                    {
                        if (footSplit.Contains(".c") || footSplit.Contains(".h"))
                        {
                            //Accurate File Name
                            strFileName = footSplit.Replace('@', ' ');
                            break;
                        }
                    }

                    if (count > 0)
                    {
                        //File is fully stored in our list, send entire list to be processed at once
                        string StrResult = WriteFunctionFile(FunctionItems, count, strFileName, strDest, baseFileName);

                        if (StrResult == "OK")
                        {
                            FunctionItems = null;
                            FunctionItems = new List<string>();

                            Console.WriteLine(strFileName);
                        }
                        else
                        {
                            Console.WriteLine("An Error Occurred");
                            Console.ReadLine();

                        }
                    }
                    else
                    {
                        FunctionItems = null;
                        FunctionItems = new List<string>();
                    }

                    FunctionItems.Add(strLineText);

                    count++;
                    strlastFoot = strLineText;
                }
                else
                {
                    FunctionItems.Add(strLineText);
                }
            }
            Console.WriteLine("=============================Completed=============================");
            Console.Read();
        }


        public static String WriteFunctionFile(List<string> FunctionLines, int intFunctionCount, string strFileName, string strDest, string baseFile)
        {
            //Parse the list we passed in to create valid code files 
            if (strFileName.TrimEnd().EndsWith("c") || strFileName.TrimEnd().EndsWith("h"))
            {
                using (StreamWriter file = new StreamWriter(strDest + strFileName))
                {
                    foreach (var item in FunctionLines)
                    {
                        file.WriteLine(item);
                    }
                }
            }

            AddStrategyToDatabase(FunctionLines, intFunctionCount, strFileName,baseFile);
            return "OK";
        }

        public static String AddStrategyToDatabase(List<string> StrategyLines, int intFunctionCount, string strFileName, string baseFile)
        {
            try
            {
                String strAbstract = "";
                string StrFileText = "";
                bool isCommentText = false;
                foreach (var item in StrategyLines)
                {
                    if (item.StartsWith(@"/*##########"))
                    {
                        isCommentText = true;
                    }

                    if (item.StartsWith(@"**#########"))
                    {
                        isCommentText = false;
                        strAbstract += item;
                        break;
                    }

                    if (isCommentText)
                    {
                        strAbstract += item;
                    }

                }

                foreach (var itemText in StrategyLines)
                {
                    StrFileText += itemText;
                }

                string formattedAbstract;

                if (strAbstract != null && strAbstract.ToUpper().Contains("ABSTRACT"))
                {
                    formattedAbstract = strAbstract.Substring(strAbstract.ToUpper().IndexOf("ABSTRACT"));
                    formattedAbstract = formattedAbstract.Replace("#", "").Replace("*", "").Replace("Abstract", "").Replace("ABSTRACT", "").Replace(":","");
                    formattedAbstract.TrimStart(' ');



                    RegexOptions options = RegexOptions.None;
                    Regex regex = new Regex("[ ]{2,}", options);
                    formattedAbstract = regex.Replace(formattedAbstract, " ");


                }
                else
                {
                    formattedAbstract = "";
                }

                Ford_ECUDataSetTableAdapters.StrategiesTableAdapter stratAdapter = new Ford_ECUDataSetTableAdapters.StrategiesTableAdapter();
                stratAdapter.Insert(baseFile, formattedAbstract, strFileName, StrFileText);
            }
            catch (Exception)
            {

                throw;
            }

            return "";
        }
    }
}
